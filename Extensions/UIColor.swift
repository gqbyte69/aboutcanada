
import UIKit

extension UIColor {

    class func rgb(red redValue: CGFloat, green greenValue: CGFloat, blue blueValue: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: 1.0)
    }
}
