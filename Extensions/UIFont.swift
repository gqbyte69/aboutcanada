
import UIKit

extension UIFont {

    class func acFont(withWeight weight: FONT_WEIGHT, andSize size: CGFloat) -> UIFont! {
        switch weight {
        case .REGULAR:
            return UIFont(name: "SourceSansPro-Regular", size: size)!
        case .BOLD:
            return UIFont(name: "SourceSansPro-Bold", size: size)!
        case .LIGHT:
            return UIFont(name: "SourceSansPro-Light", size: size)!
        case .SEMI_BOLD:
            return UIFont(name: "SourceSansPro-Semibold", size: size)!
        }
    }
}
