
import UIKit

class ACBaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the custom color of the nav bar
        navigationBar.barTintColor = UIColor.rgb(red: 131, green: 2, blue: 6)

        // Set the custom font and color of the nav bar title
        navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.acFont(withWeight: .BOLD, andSize: 19),
                NSAttributedStringKey.foregroundColor : UIColor.white
        ]
    }

}
