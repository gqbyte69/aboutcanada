
import UIKit

class ACBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // We make sure that the nav bar is visible
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let navCon = navigationController {
            // Again, we make sure that the nav bar is visible
            navCon.setNavigationBarHidden(false, animated: true)
            navCon.navigationBar.isTranslucent = false
        }

        // This removes the text of the back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backItem
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        view.endEditing(true)
    }

    // We use this to make sure that we always push with a valid navigation controller
    func pushVC(viewController: UIViewController) {

        if let navCon = navigationController {
            navCon.pushViewController(viewController, animated: true)
        }

        // We do nothing if we have no navigation controller
    }

}
