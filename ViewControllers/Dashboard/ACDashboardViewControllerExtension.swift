
import UIKit

extension ACDashboardViewController {

    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {

        // Resize cell width depending on orientation
        if let flowLayout = cvFeed.collectionViewLayout as? UICollectionViewFlowLayout {

            // Added 20px of padding on the left and right side of the cell
            let w = cvFeed.frame.width - 20
            flowLayout.estimatedItemSize = CGSize(width: w, height: 10)
        }

        cvFeed.performBatchUpdates(nil, completion: nil)
    }

    //MARK: - UICollectionViewDelegate -
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc: ACDetailsViewController = VC().DETAILS
        vc.currentFeed = arrayFeeds[indexPath.row]
        pushVC(viewController: vc)
    }

    //MARK: - UICollectionViewDataSource -
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFeeds.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ACFeedCellID", for: indexPath) as! ACFeedCell
        let feed: ACFeedModel = arrayFeeds[indexPath.row]

        // Assign the feed property to cell. Cell does the changing of the elements.
        cell.feed = feed

        return cell
    }
}
