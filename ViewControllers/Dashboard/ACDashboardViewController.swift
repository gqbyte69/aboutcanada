
import UIKit
import SDWebImage

class ACDashboardViewController: ACBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var cvFeed: UICollectionView!

    var rcFeed: UIRefreshControl!
    var arrayFeeds: [ACFeedModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = APP_NAME

        // Set initial estimated size of the cell
        if let flowLayout = cvFeed.collectionViewLayout as? UICollectionViewFlowLayout {
            let w = cvFeed.frame.width - 20
            flowLayout.estimatedItemSize = CGSize(width: w, height: 10)
        }

        // Initialize and set the UIRefreshControl
        rcFeed = UIRefreshControl()
        rcFeed.attributedTitle = NSAttributedString(string: "load_pull_refresh".localized())
        rcFeed.addTarget(self, action: #selector(getFeed(sender:)), for: .valueChanged)
        cvFeed.addSubview(rcFeed)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        getFeed(sender: nil)
    }

    @objc func getFeed(sender: UIRefreshControl?) {

        let restManager: ACRestManager = ACRestManager()

        // Fetch the data
        restManager.consumeAPI(
            usingUrl: FEED_URL,
            withVerb: REST_VERBS.GET,
            onSuccess: { (data) in
                self.endRefresh()

                // Check if we have the expected key
                if data[ROWS].exists() {

                    self.arrayFeeds.removeAll()

                    // Iterate through all the JSON objects inside the "rows" key
                    for obj in data[ROWS].array! {
                        let feed: ACFeedModel = ACFeedModel.init(data: obj)
                        self.arrayFeeds.append(feed)
                    }

                    self.cvFeed.reloadData()
                } else {
                    log.error("Unexpected response.")
                }
        }) { (error) in
            self.endRefresh()
            log.error(error.message)
        }
    }

    func endRefresh() {
        if rcFeed.isRefreshing {
            rcFeed.endRefreshing()
        }
    }
}
