
import UIKit
import SDWebImage

class ACDetailsViewController: ACBaseViewController {

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgPicture: UIImageView!

    var currentFeed: ACFeedModel!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.title = currentFeed.title
        lblDescription.text = currentFeed.desc

        imgPicture.image = IMG().PLACEHOLDER

        // Set the image if we have a valid URL
        if let urlImg = currentFeed.imageUrl {
            if urlImg.count != 0 {
                imgPicture.sd_setShowActivityIndicatorView(true)
                imgPicture.sd_setIndicatorStyle(.gray)
                imgPicture.sd_setImage(with: URL(string : urlImg), placeholderImage: IMG().PLACEHOLDER)
            }
        }
    }
}
