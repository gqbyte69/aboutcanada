
import SwiftyJSON

class ACFeedModel: ACBaseModel {

    var title: String!
    var desc: String!
    var imageUrl: String?

    init(data: JSON) {
        title = data[TITLE].string
        desc = data[DESCRIPTION].string
        imageUrl = data[IMAGEHREF].string
    }
}
