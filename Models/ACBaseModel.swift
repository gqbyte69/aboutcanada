
import Foundation

class ACBaseModel: NSObject {

    override init(){
        super.init()

    }

    // For debugging purposes only
    func printMembers() {
        let mirrored_object = Mirror(reflecting: self)

        for (_, attr) in mirrored_object.children.enumerated() {
            if let property_name = attr.label as String? {
                log.info("[\(property_name)] = [\(attr.value)]")
            }
        }
    }

}
