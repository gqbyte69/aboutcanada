
import UIKit
import SDWebImage

class ACFeedCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPicture: UIImageView!

    // Set image and text when feed property is assigned
    var feed: ACFeedModel! {
        didSet {
            lblTitle.text = feed.title

            imgPicture.image = IMG().PLACEHOLDER

            if let urlImg = feed.imageUrl {
                if urlImg.count != 0 {
                    imgPicture.sd_setShowActivityIndicatorView(true)
                    imgPicture.sd_setIndicatorStyle(.gray)
                    imgPicture.sd_setImage(with: URL(string : urlImg), placeholderImage: IMG().PLACEHOLDER)
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    // This sets the needed height of the collection view cell
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()

        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame = layoutAttributes.frame
        frame.size.height = ceil(size.height)
        layoutAttributes.frame = frame

        return layoutAttributes
    }

}
