
enum FONT_WEIGHT {
    case REGULAR
    case BOLD
    case SEMI_BOLD
    case LIGHT
}

enum REST_VERBS {
    case POST
    case GET
    case PUT
    case DELETE
}

