
import UIKit

struct SB {
    static let MAIN = UIStoryboard(name: "Main", bundle: nil)
}

struct VC {
    let DASHBOARD = SB.MAIN.instantiateViewController(withIdentifier: "ACDashboardViewControllerID") as! ACDashboardViewController
    let DETAILS = SB.MAIN.instantiateViewController(withIdentifier: "ACDetailsViewControllerID") as! ACDetailsViewController
}

struct IMG {
    let PLACEHOLDER = UIImage(named:"placeholder")!
}
