
import Foundation
import Alamofire
import SwiftyJSON

typealias GenericCompletion = (() -> Swift.Void)?
typealias ApiCompletion     = ((_ success: Bool, _ message: String, _ data: Any?) -> Swift.Void)?
typealias OnSuccessHandler  = ((_ data: JSON) -> Swift.Void)?
typealias OnFailHandler     = ((_ data: ACRestErrorModel) -> Swift.Void)?

class ACRestManager: NSObject {

    override init() {

    }

    // A retry mechanism when REST call fails. Default retry count is 3. Will prompt user to retry.
    fileprivate func retry(
        numberOfTimes: Int,
        task: @escaping (_ success: @escaping (JSON) ->(), _ failure: @escaping (String, Int) -> ()) -> Void,
        success: OnSuccessHandler = nil,
        failure: OnFailHandler = nil
        )
    {

        task({ (response) in
            success?(response)
        }) { (error, code) in

            var counter = numberOfTimes
            if counter > 1 {
                log.error("RETRYING....(\(numberOfTimes))")

                self.retry(numberOfTimes: counter - 1, task: task, success: success, failure: failure)
            } else {
                log.error("RETRY COUNT MAXED.")

                ACAlert.genericChoice(
                    withMessage: "retry".localized(),
                    withTitle: APP_NAME,
                    withCancelTItle: "no".localized(),
                    withOkTItle: "yes".localized(),
                    onOk: {
                        counter = 3
                        self.retry(numberOfTimes: counter - 1, task: task, success: success, failure: failure)
                },
                    onCancel: {
                        let errorObjc: ACRestErrorModel = ACRestErrorModel()
                        errorObjc.message = error
                        errorObjc.code = code
                        failure?(errorObjc)
                })
            }
        }
    }

    internal func consumeAPI(
        usingUrl url: String,
        withVerb restVerb: REST_VERBS,
        withParameters params: NSDictionary? = nil,
        onSuccess: OnSuccessHandler = nil,
        onFail: OnFailHandler = nil
        ) {

        // A unique ID specifically for debugging purposes to identify the request-response pair
        let uuid = UUID().uuidString

        var localVerb : HTTPMethod!

        switch restVerb {
        case .POST:
            localVerb = .post
            break
        case .GET:
            localVerb = .get
            break
        case .PUT:
            localVerb = .put
            break
        case .DELETE:
            localVerb = .delete
            break
        }

        if params != nil {
            log.debug("\n\nVERB: [\(restVerb)]\nURL:  [\(url)]\nREQUEST: [\(uuid)]: \nPARAMS:  [\(params!)]\n")
        } else {
            log.debug("\n\nVERB: [\(restVerb)]\nURL:  [\(url)]\nREQUEST: [\(uuid)]: \nPARAMS:  [none]\n")
        }

        var requestParam : Parameters? = nil

        // If there are parameters, we fix them in order to conform to the "Parameters" object of Alamofire
        if let parameters = params {
            let res : JSON = JSON.init(parameters)

            var dict = [String:Any]()
            for (key, value) in res {
                dict[key] = value.rawString()
            }

            requestParam = dict
        }

        self.retry(
            numberOfTimes: 3,
            task: { (onSuccess, onFail) in
                Alamofire.request(
                    url,
                    method: localVerb,
                    parameters: requestParam,
                    encoding: URLEncoding.default,
                    headers: nil).responseString { response in
                        let responseString: String = response.result.value!
                        log.debug("\n\nRESPONSE [\(uuid)]: \n\(responseString)\n")

                        switch response.result {
                        case .success(_):
                            let jsonResponse: JSON = JSON.init(parseJSON: responseString)
                            onSuccess(jsonResponse)
                            break

                        case .failure(let error):

                            if let err = error as? URLError {

                                switch err.code {
                                case URLError.Code.notConnectedToInternet:
                                    // No internet connection
                                    onFail("error_no_internet".localized(), 500)
                                    break
                                case URLError.Code.timedOut:
                                    onFail("error_request_timeout".localized(), 500)
                                    break
                                default:
                                    onFail("Server error: (\(err.errorCode))", err.errorCode)
                                    break
                                }

                            } else {
                                // Other failures
                                log.error("ERROR [\(uuid)]: \(response.result.error!)\n\nACTUAL DATA: \(response.result.value ?? "")")
                                onFail("error_invalid_server_response".localized(), 9999)
                            }

                            break
                        }
                }

        },
            success: onSuccess,
            failure: onFail
        )

    }
}


