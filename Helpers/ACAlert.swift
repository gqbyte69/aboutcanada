
import UIKit

class ACAlert: NSObject {

    // Shows a generic alert controller with 2 options
    class func genericChoice(
        inVc vc: UIViewController? = UIApplication.topViewController(),
        withMessage message: String? = nil,
        withTitle title: String? = nil,
        withCancelTItle titleCancel: String!,
        withOkTItle titleOk: String!,
        onOk: (() -> Swift.Void)? = nil,
        onCancel: (() -> Swift.Void)? = nil
        ) {

        let alertController: UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)

        // We won't add a button if the title is set to nil
        if let optionOk = titleOk {
            let actionOk: UIAlertAction = UIAlertAction.init(title: optionOk, style: .default) { (sender) in
                onOk?()
            }
            alertController.addAction(actionOk)
        }

        if let optionCancel = titleCancel {
            let actionCancel: UIAlertAction = UIAlertAction.init(title: optionCancel, style: .cancel) { (sender) in
                onCancel?()
            }
            alertController.addAction(actionCancel)
        }

        vc?.view.endEditing(true)
        vc?.present(alertController, animated: true, completion: nil)
    }
}
